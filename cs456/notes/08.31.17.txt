DFA (Deterministic Finite Automatan)
  M = (Q, sigma, delta, Q0, F)
    Q = finite set of internal states
    sigma = finite set of symbols (called alphabet)
    delta = finite set of transition functions, where
            the cartesian product Q x sigma -> Q
    Q0 = initial state, (Q0 exists in Q)
    F = finite set of final state(s), (F is a subset of Q)
