#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef BS
#define BS 3 // macro representing board size
#endif

void clear_table(char board[BS][BS]);
void generate_player2_move(char board[BS][BS], int* row, int* col);
bool check_table_full(char board[BS][BS]);
int check_three_in_a_row(char board[BS][BS]);
void display_table(char board[BS][BS]);
bool check_legal_option(char board[BS][BS], unsigned int row, unsigned int col);
void update_table(char board[BS][BS], unsigned int row, unsigned int col);

int main()
{
  int turn = 0;
  int row, col;
  char board[BS][BS];
  clear_table(board);

  do 
  {
    printf("The current state of the game is:\n");
    display_table(board);
    
    if( turn % 2 == 0 )
    {
      do
      {
        printf("Player 1 enter your selection [row,col]: ");
        scanf("%d,%d", &row, &col);        
      } while( !check_legal_option(board, row - 1, col - 1) );
      
      update_table(board, row - 1, col - 1);
      turn++;
    }
    else
    {
      generate_player2_move(board, &row, &col);
      printf("Player 2 has entered [row,col]: %d,%d\n", row + 1, col + 1);
      turn++;
    }
    
  } while( !check_table_full(board) );

  return 0;
}

void clear_table(char board[BS][BS])
{
  for( int row = 0; row < BS; row++ )
  {
    for( int col = 0; col < BS; col++ )
    {
      board[row][col] = '_';
    }
  }
}

// generates move for computer and places it in board as 'O'
void generate_player2_move(char board[BS][BS], int* row, int* col)
{
  bool found_spot = false;
  srand(time(NULL));
  
  while( !found_spot )
  {
    *row = rand() % 3;
    *col = rand() % 3;
    if( board[*row][*col] == '_' )
    {
      board[*row][*col] = 'O';
      found_spot = true;
    }
  }
}

bool check_table_full(char board[BS][BS])
{  
  for( int row = 0; row < BS; row++ )
  {
    for( int col = 0; col < BS; col++ )
    {
      if( board[row][col] == '_' )
      {
        return false;
      }
    }
  }
  
  return true;
}

int check_three_in_a_row(char board[BS][BS])
{
  //TODO
  return 0;
}

void display_table(char board[BS][BS])
{
  for( int row = 0; row < BS; row++ )
  {
    for( int col = 0; col < BS; col++ )
    {
      printf("%c ", board[row][col]);
    }
    printf("\n");
  }
  printf("\n");
}

bool check_legal_option(char board[BS][BS], unsigned int row, unsigned int col)
{
  if( row >= BS || col >= BS )
  {
    return false;
  }
  else if( board[row][col] != '_' )
  {
    return false;
  }
  else
  {
    return true;
  }
}

// same as generate_player2_move but for human player
void update_table(char board[BS][BS], unsigned int row, unsigned int col)
{
  // this check may be redundant but it also acts as a failsafe
  // for other possible main implementations
  if( check_legal_option(board, row, col) )
  {
    board[row][col] = 'X';
  }
}
