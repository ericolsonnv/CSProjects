#include <stdio.h>
#include <stdlib.h>

void read_header(FILE* fstream, int* width, int* height);
void read_image_body(FILE* fstream, int** image);
int** read_image(char* filename, int* width, int* height);
int get_image_pixel(int** image, int x, int y);
void set_image_pixel(int** image, int x, int y, int value);

int main(int argc, char* argv[])
{
  FILE* fstream;
  int x, y; // variables to store user input
  int width, height; // variables to store width and height of image
  int** image; // double int pointer to store image

  // verify that the correct number of arguments were supplied
  if( argc != 2 )
  {
    printf("Invalid number of arguments.\n");
    return 1;
  }

  // read in the image
  image = read_image(argv[1], &width, &height);

  // loop forever
  do
  {
    // read in x and y values from user
    printf("Please enter a pixel value (x, y): ");
    scanf("%d, %d", &x, &y);

    // if x or y are invalid, stop that shit
    if( x < 0 || x > width || y < 0 || y > height )
    {
      break;
    }

    // get and set image pixel at specified location
    printf("%d\n", get_image_pixel(image, x, y));
    set_image_pixel(image, x, y, 0);

  } while( 1 );

  // re-write the modified image back to the file 
  fstream = fopen(argv[1], "w+");
  
  fprintf(fstream, "P2\n");
  fprintf(fstream, "# %s\n", argv[1]);
  fprintf(fstream, "%d %d\n", width, height);
  fprintf(fstream, "15\n");
  
  for( int row = 0; row < height; row++ )
  {
    for( int col = 0; col < width; col++ )
    {
      if( col == 0 )
      {
        fprintf(fstream, "%d", image[row][col]);        
      }
      else
      {
        fprintf(fstream, "%3d", image[row][col]);
      }
    }
    fprintf(fstream, "\n");
  }
  
  fclose(fstream);
  
  return 0;
}

// this function just throws out the bullshit and reads in the width/height
void read_header(FILE* fstream, int* width, int* height)
{
  char *line = NULL;
  size_t len = 0;

  // throw out the first two lines
  for( int i = 0; i < 2; i++ )
  {
    getline(&line, &len, fstream);
  }

  // read in the width and height, separated by a space
  fscanf(fstream, "%d %d", width, height);

  // throw out the remainder of the line as well as the following line
  for( int i = 0; i < 2; i++ )
  {
    line = NULL;
    len = 0;
    getline(&line, &len, fstream);
  }

  // free up the memory allocated to line
  free(line);

  // the important values are returned by reference
  return;
}

// this is very poor design
// this function has no way of knowing the size of the array
// which can lead to very easily going out of bounds
// it really should take the width and height as parameters
// without them it is very difficult to handle errors gracefully
void read_image_body(FILE* fstream, int** image)
{
  char* line = NULL; // char pointer to store line into
  size_t len = 0;
  int row = 0;
  int col = 0;

  // encapsulate loop to iterate through every remaining line
  while( getline(&line, &len, fstream) != -1 )
  {    
    // iterate through the line until newline character is encountered
    while( *line != '\n' )
    {
      // if the current character at line[0] is a space, move on to next char
      if( *line == ' ' )
      {
        line++;
      }
      // if it's not, read in the integer, and then move on to next char
      else
      {
        sscanf(line, "%d", &image[row][col]);
        col++;

        // move cursor to nearest whitespace
        while( *line != ' ' && *line != '\n' )
        {
          line++;
        }
      }
    }
    // increment row counter and reset values for getline
    row++;
    line = NULL;
    len = 0; col = 0;
  }

  // free up the memory allocated to line
  free(line);

  return;
}

// this function reads in the image by calling the above helper functions
// it will then return the double int pointer that represents the image
// memory allocation is handled in this function
int** read_image(char* filename, int* width, int* height)
{
  FILE* fstream; // file pointer to use as stream
  int** image; // double int pointer to store image data in

  // open the file with read/write permissions
  fstream = fopen(filename, "r+");

  // read in the width and height
  read_header(fstream, width, height);

  // allocate memory based on height
  image = (int**) calloc(*height, sizeof(int*));

  // loop through each index in height
  for( int i = 0; i < *height; i++)
  {
    // allocate memory based on width
    image[i] = (int*) calloc(*width, sizeof(int));
  }

  // read in image data
  read_image_body(fstream, image);

  // close the file
  fclose(fstream);

  // return the image
  return image;
}

// this function returns the value found at image[x][y]
// this function assumes that only valid indices will be passed
// any and all error checking regarding bounds MUST be handled prior to calling
int get_image_pixel(int** image, int x, int y)
{
  return image[y][x];
}

// this function sets the pixel at image[x][y] to value
// error handling in this function is the same as the above function
void set_image_pixel(int** image, int x, int y, int value)
{
  image[y][x] = value;
  return;
}
