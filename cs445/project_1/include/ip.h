#ifndef CLASS_IP_H
#define CLASS_IP_H

#include <cstdlib>
#include <iostream>
#include <string>
#include <ctime>

class ip {
  public:
    ip();
    ~ip();

    ip insert(int first_octet, int second_octet);
    ip generate();
    void clear();

    int get_first_octet();
    int get_second_octet();
    bool is_marked();
    std::string get_address();

    void operator = (const ip &net);

  private:
    int first_octet, second_octet;
    bool marked;
    std::string address;
};

#endif // CLASS_IP_H