#include <iostream>

#include "../include/ip.h"
#include "../include/network.h"

int node_sample(Network net, int p);
int edge_sample(Network net, int p);

int main() {
  int index;
  ip ip1;
  Network net10, net20, net30;

  std::srand(time(NULL));

  for ( index = 0; index < 10; index++ ) {
    ip1.generate();
    net10.insert(ip1);
  }

  for ( index = 0; index < 20; index++ ) {
    ip1.generate();
    net20.insert(ip1);
  }

  for ( index = 0; index < 30; index++ ) {
    ip1.generate();
    net30.insert(ip1);
  }

  std::cout << "(node_sample) 10 routers, p = 0.2: " << node_sample(net10, 20) << std::endl;
  std::cout << "(node_sample) 10 routers, p = 0.4: " << node_sample(net10, 40) << std::endl;
  std::cout << "(node_sample) 10 routers, p = 0.6: " << node_sample(net10, 60) << std::endl;
  std::cout << "(node_sample) 10 routers, p = 0.8: " << node_sample(net10, 80) << std::endl;

  std::cout << "(node_sample) 20 routers, p = 0.2: " << node_sample(net20, 20) << std::endl;
  std::cout << "(node_sample) 20 routers, p = 0.4: " << node_sample(net20, 40) << std::endl;
  std::cout << "(node_sample) 20 routers, p = 0.6: " << node_sample(net20, 60) << std::endl;
  std::cout << "(node_sample) 20 routers, p = 0.8: " << node_sample(net20, 80) << std::endl;

  std::cout << "(node_sample) 30 routers, p = 0.2: " << node_sample(net30, 20) << std::endl;
  std::cout << "(node_sample) 30 routers, p = 0.4: " << node_sample(net30, 40) << std::endl;
  std::cout << "(node_sample) 30 routers, p = 0.6: " << node_sample(net30, 60) << std::endl;
  std::cout << "(node_sample) 30 routers, p = 0.8: " << node_sample(net30, 80) << std::endl;

  std::cout << std::endl;

  std::cout << "(edge_sample) 10 routers, p = 0.2: " << edge_sample(net10, 20) << std::endl;
  std::cout << "(edge_sample) 10 routers, p = 0.4: " << edge_sample(net10, 40) << std::endl;
  std::cout << "(edge_sample) 10 routers, p = 0.6: " << edge_sample(net10, 60) << std::endl;
  std::cout << "(edge_sample) 10 routers, p = 0.8: " << edge_sample(net10, 80) << std::endl;

  std::cout << "(edge_sample) 20 routers, p = 0.2: " << edge_sample(net20, 20) << std::endl;
  std::cout << "(edge_sample) 20 routers, p = 0.4: " << edge_sample(net20, 40) << std::endl;
  std::cout << "(edge_sample) 20 routers, p = 0.6: " << edge_sample(net20, 60) << std::endl;
  std::cout << "(edge_sample) 20 routers, p = 0.8: " << edge_sample(net20, 80) << std::endl;

  std::cout << "(edge_sample) 30 routers, p = 0.2: " << edge_sample(net30, 20) << std::endl;
  std::cout << "(edge_sample) 30 routers, p = 0.4: " << edge_sample(net30, 40) << std::endl;
  std::cout << "(edge_sample) 30 routers, p = 0.6: " << edge_sample(net30, 60) << std::endl;
  std::cout << "(edge_sample) 30 routers, p = 0.8: " << edge_sample(net30, 80) << std::endl;

  return 0;
}

int node_sample(Network net, int p) {
  int x, index;
  int count = 0;

  for( index = 0; index < net.get_size(); index++ ) {
    x = rand() % 100;
    if( x < p ) {
      count++;
    }
  }

  return count;
}

int edge_sample(Network net, int p) {
  int x, index;
  int count = 0;

  for( index = 0; index < net.get_size(); index++ ) {
    x = rand() % 100;
    if( x < p && index % 2 != 0) {
      count++;
    }
  }

  return count;
}