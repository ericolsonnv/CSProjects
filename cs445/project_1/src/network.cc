#include "../include/network.h"

Network::Network() {
  branches = 2;
  size = 0;
  current = 0;
  hosts = NULL;
}

Network::~Network() {
  branches = 0;
  // clear();
}

void Network::insert(ip new_host) {
  int index;
  ip* hosts_copy;

  if ( size == 0 ) {
    hosts = new ip [size + 1];
    hosts[0] = new_host;
    size++;
  } else {
    hosts_copy = new ip [size + 1];
    for ( index = 0; index < size; index++ ) {
      hosts_copy[index] = hosts[index];
    }
    hosts_copy[size] = new_host;
    size++;

    delete [] hosts;
    hosts = hosts_copy;
    hosts_copy = NULL;
  }
}

bool Network::remove() {
  int index;
  ip* hosts_copy;

  if ( size == 0 ) {
    return false;
  } else if ( size == 1 ) {
    clear();
  } else {
    hosts_copy = new ip [ size - 1];
    for( index = 0; index < size - 1; index++ ) {
      hosts_copy[index] = hosts[index];
    }
    size--;
    if ( current >= size ) {
      current = size - 1;
    }

    delete [] hosts;
    hosts = hosts_copy;
    hosts_copy = NULL;
  }

  return true;
}

void Network::clear() {
  size = 0;

  if ( hosts != NULL ) {
    delete [] hosts;
    hosts = NULL;
  }
}

int Network::get_size() {
  return size;
}

bool Network::next() {
  if ( current + 1 < size ) {
    current++;
    return true;
  } else {
    return false;
  }
}

bool Network::previous() {
  if ( current != 0 ) {
    current--;
    return true;
  } else {
    return false;
  }
}

void Network::print() {
  int index;

  for ( index = 0; index < size; index++ ) {
    std::cout << "[" << index << "] " << hosts[index].get_address() << std::endl;
  }
}
