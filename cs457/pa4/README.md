# CS457 Database Management Systems
## Programming Assignment 4: Transactions
#### Sui Cheung, Eric Olson, Jake Wheeler

## Assignment Overview
"In this assignment you will write a program that allows a database user to enclose SQL statements into a transaction block. That is, you will implement the “all-or-nothing” property commonly seen in real-world database systems. This assignment assumes the metadata and data manipulations have been implemented in the first three programming assignments."

## Implementation
This database metadata management application is implemented using Python 3, and allows for the creation, deletion, and altering of databases and tables. Databases are created as subdirectories within the `databases` directory in the root of the repository. Within those directories, tables are created with the format:
```
attribute_name1 attribute_type1 | attribute_name2 attribute_type2
value1 | value2
```

Attributes that are selected from tables are loaded into table reader objects and are iterated over to compare to other tables. The joined rows are then formatted for output.

## Usage
**REQUIRES PYTHON 3.6**

**COMMENTS CANNOT BE IN THE SAME LINE AS A COMMAND**


The program can be run with: `python3.6 app.py`
For cleaner output when redirecting from a file, it is recommended to use `python3.6 app.py -s < filename`
The following commands can be used:
```
CREATE DATABASE <database name>;
CREATE TABLE <table name>(<attributes>);
USE <database name>;
INSERT into <table name> values(<values>);
UPDATE <table name> SET <attribute> = <value> WHERE <attribute> = <value>;
SELECT <attribute, *> FROM <table name>;
DELETE from <table name> WHERE <attribute> = <value>;
DROP DATABASE <database name>;
DROP TABLE <table name>;
ALTER <table name> ADD <attribute_name> <attribute_type>;
.EXIT
--<Comment>
```

### Sample Input
```
-- CS457 PA4
-- This script includes the commands to be executed by two processes, P1 and P2
-- On P1:CREATE DATABASE CS457_PA4;
USE CS457_PA4;
create table Flights(seat int, status int);
insert into Flights values(22,0);-- seat 22 is available
insert into Flights values(23,1);-- seat 23 is occupied
begin transaction;
update flights set status = 1 where seat = 22;
-- On P2:
USE CS457_PA4;
select * from Flights;
begin transaction;
update flights set status = 1 where seat = 22;
commit; --there should be nothing to commit; it's an "abort"
select * from Flights;
-- On P1:
commit; --persist the change to disk
select * from Flights;
-- On P2:
select * from Flights;
```

### Expected output
```
-- On P1:
-- Database CS457_PA4 created.
-- Using database CS457_PA4.
-- Table Flights created.
-- 1 new record inserted.
-- 1 new record inserted.
-- Transaction starts.
-- 1 record modified.
-- Transaction committed.
-- seat int|status int
-- 22|1
-- 23|1
-- On P2:
-- Using database CS457_PA4.
-- seat int|status int
-- 22|0
-- 23|1
-- Transaction starts.
-- Error: Table Flights is locked!
-- Transaction abort.
-- seat int|status int
-- 22|0
-- 23|1
-- seat int|status int
-- 22|1
-- 23|1
```
