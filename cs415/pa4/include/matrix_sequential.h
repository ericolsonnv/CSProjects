#ifndef MATRIX_SEQUENTIAL_H
#define MATRIX_SEQUENTIAL_H

#include <cstdlib>
#include <fstream>
#include <iostream>

#include "matrix.h"
#include "timer.h"

#define PROG_NAME "matrix_sequential"

int read_header(char* filename);

#endif // MATRIX_SEQUENTIAL_H