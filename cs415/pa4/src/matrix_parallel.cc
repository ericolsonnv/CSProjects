#include "../include/matrix_parallel.h"

int main(int argc, char* argv[]) {
  int world_size, world_rank;
  int mesh_index, calc_index, world_index;
  int a_size, b_size;
  int mesh_size, mesh_start, mesh_row;
  int left, up;
  double elapsed_time;
  struct timeval timer;
  matrix matrix_a, matrix_b, matrix_c;
  matrix *matrices_a, *matrices_b, *matrices_c;
  MPI_Status status;

  // verify that the correct number of arguments were supplied
  if ( argc != 3 ) {
    std::cerr << PROG_NAME << ": Invalid number of arguments supplied" << std::endl;
    return 1;
  }

  // initialize the mpi environment
  MPI_Init(NULL,NULL);

  // get world information
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // verify a valid world size
  if ( !is_perfect_square(world_size) ) {
    std::cerr << PROG_NAME << ": Invalid world size supplied" << std::endl;
    return 1;
  }

  // read in the sizes
  a_size = matrix_a.open(argv[1]);
  b_size = matrix_b.open(argv[2]);
  matrix_c.resize(b_size, a_size);

  // Print initial matrices
  if ( world_rank == 0 ) {
    if ( PRINT ) {
      std::cout << "Matrix A:" << std::endl;
      matrix_a.print();
      std::cout << "Matrix B" << std::endl;
      matrix_b.print();
    }
  }

  mesh_size = (int)(a_size / sqrt(world_size));

  if ( world_rank == 0 ) {
    // verify that the matrices can be multiplied
    if ( matrix_a.get_cols() != matrix_b.get_rows() ) {
      std::cerr << PROG_NAME << ": matrices cannot be multiplied" << std::endl;
      MPI_Abort(MPI_COMM_WORLD, 1);
    }


    // perform initial shift
    mesh_start = (a_size / sqrt(world_size)) * (sqrt(world_size) - 1);
    for ( mesh_index = 0; mesh_index < mesh_size; mesh_index++ ) {
      matrix_a.shift_row(mesh_start + mesh_index, (-1) * mesh_size);
      matrix_b.shift_col(mesh_start + mesh_index, (-1) * mesh_size);
    }

    // split the matrices into sub-matrices
    matrices_a = matrix_a.split(world_size);
    matrices_b = matrix_b.split(world_size);
    matrices_c = matrix_c.split(world_size);

    // verify the success of the split
    if ( matrices_a == NULL || matrices_b == NULL ) {
      std::cerr << "failed to split one of the matrices" << std::endl;
      return 1;
    }

    // wait here for other processes
    MPI_Barrier(MPI_COMM_WORLD);

    // send initial matrices to each process
    for ( world_index = 1; world_index < world_size; world_index++ ) {
      MPI_Send(&(matrices_a[world_index].data[0][0]), matrices_a[world_index].get_rows() * matrices_a[world_index].get_cols(), MPI_INT, world_index, 0, MPI_COMM_WORLD);
      MPI_Send(&(matrices_b[world_index].data[0][0]), matrices_b[world_index].get_rows() * matrices_b[world_index].get_cols(), MPI_INT, world_index, 1, MPI_COMM_WORLD);
    }

    // resize and assign own local matrix
    matrix_a.resize(mesh_size, mesh_size);
    matrix_b.resize(mesh_size, mesh_size);

    matrix_a = matrices_a[0];
    matrix_b = matrices_b[0];

  } else {
    // wait for master to initialize everything
    MPI_Barrier(MPI_COMM_WORLD);

    // receive initial sub matrix
    MPI_Recv(&(matrix_a.data[0][0]), mesh_size * mesh_size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
    MPI_Recv(&(matrix_b.data[0][0]), mesh_size * mesh_size, MPI_INT, 0, 1, MPI_COMM_WORLD, &status);
  }

  // initialize counter
  calc_index = 0;

  // calculate neighbors
  mesh_row = world_rank / sqrt(world_size);
  left = absmod(world_rank - 1, (int)sqrt(world_size)) + (mesh_row * sqrt(world_size));
  up = absmod(world_rank - (int)sqrt(world_size), world_size);

  // wait for everyone to catch up
  MPI_Barrier(MPI_COMM_WORLD);

  // start timer
  if ( world_rank == 0 ) {
    gettimeofday(&timer, NULL);
  }

  // perform first multiplication
  matrix_c = matrix_a * matrix_b;

  // Shift the sub matrices and calculate.
  for ( calc_index = 1; calc_index < sqrt(world_size); calc_index++ ) {
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Sendrecv_replace(&(matrix_a.data[0][0]), mesh_size * mesh_size, MPI_INT, left, 0, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Sendrecv_replace(&(matrix_b.data[0][0]), mesh_size * mesh_size, MPI_INT, up, 0, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    MPI_Barrier(MPI_COMM_WORLD);

    matrix_c = matrix_c + (matrix_a * matrix_b);

    // MPI_Barrier(MPI_COMM_WORLD);
    calc_index++;
  }

  // end timer
  if ( world_rank == 0 ) {
    elapsed_time = get_time(timer);
    std::cout << "time taken: " << elapsed_time << std::endl;
  }

  // gather matrices back together
  if( world_rank == 0 ) {
    matrices_c[0] = matrix_c;
  }
  for (world_index = 1; world_index < world_size; world_index++ ) {
    if ( world_rank == 0 ) {
      MPI_Recv(&(matrices_c[world_index].data[0][0]), mesh_size * mesh_size, MPI_INT, world_index, 0, MPI_COMM_WORLD, &status);
    } else if ( world_rank == world_index ) {
      MPI_Send(&(matrix_c.data[0][0]), mesh_size * mesh_size, MPI_INT, 0, 0, MPI_COMM_WORLD);
    }
  }

  // merge and print
  if ( world_rank == 0 ) {
    matrix_c.merge(matrices_c, world_size);

    if ( PRINT ) {
      matrix_c.print();
    }
  }


  // finalize
  MPI_Finalize();

  // free up allocated memory
  if ( world_rank == 0 ) {
    delete [] matrices_a;
    delete [] matrices_b;
    delete [] matrices_c;
    matrices_a = matrices_b = matrices_c = NULL;
  }

  return 0;
}
