#include "../include/matrix_sequential.h"

int main(int argc, char* argv[]) {
  int size;
  double elapsed_time;
  struct timeval timer;
  matrix matrix_a, matrix_b, matrix_c;

  if ( argc != 3 ) {
    std::cerr << PROG_NAME << ": Invalid number of arguments supplied" << std::endl;
    return 1;
  }

  size = atoi(argv[1]);
  matrix_a.resize(size, size);

  size = atoi(argv[2]);
  matrix_b.resize(size, size);

  if ( matrix_a.get_cols() != matrix_b.get_rows() ) {
    std::cerr << PROG_NAME << ": matrices cannot be multiplied" << std::endl;
    return 1;
  }

  srand(time(NULL));
  matrix_a.generate();
  matrix_b.generate();

  gettimeofday(&timer, NULL);
  matrix_c = matrix_a * matrix_b;
  elapsed_time = get_time(timer);
  std::cout << elapsed_time;

  // std::cout << "Matrix A:" << std::endl;
  // matrix_a.print();
  // std::cout << std::endl << "Matrix B:" << std::endl;
  // matrix_b.print();
  // std::cout << std::endl << "Matrix C:" << std::endl;
  // matrix_c.print();

  return 0;
}

// deprecated function to read in matrix from file
int read_header(char* filename) {
  int header;
  std::ifstream fin;

  fin.open(filename);
  if ( fin.fail() ) {
    std::cerr << "matrix_sequential.cc: Failed to open file " << filename << std::endl;
    return -1;
  }
  fin >> header;
  fin.close();

  return header;
}