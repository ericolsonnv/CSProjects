#include "../include/matrix.h"

#define MATRIX_SIZE 4
#define NUM_MATRICES 4

int main() {
  std::cout << "Creating Matrix using default constructor" << std::endl;
  matrix matrix_default;
  std::cout << "Default constructor completed successfully" << std::endl;
  std::cout << std::endl;

  std::cout << "Testing to see if the matrix is empty (should be yes)" << std::endl;
  if ( matrix_default.is_empty() ) {
    std::cout << "The matrix is empty" << std::endl;
  } else {
    std::cout << "The matrix is NOT empty" << std::endl;
  }
  std::cout << std::endl;

  std::cout << "Resizing the matrix to be 4x4" << std::endl;
  matrix_default.resize(MATRIX_SIZE, MATRIX_SIZE);
  std::cout << "Matrix now has " << matrix_default.get_rows()
            << " rows, and " << matrix_default.get_cols() << " columns" << std::endl;
  std::cout << std::endl;

  std::cout << "Setting matrix to have values 0 - " << MATRIX_SIZE * MATRIX_SIZE - 1 << std::endl;
  for ( int row_index = 0; row_index < matrix_default.get_rows(); row_index++ ) {
    for ( int col_index = 0; col_index < matrix_default.get_cols(); col_index++ ) {
      matrix_default.set_element(row_index, col_index, MATRIX_SIZE * row_index + col_index);
    }
  }
  matrix_default.print();
  std::cout << std::endl;

  std::cout << "Creating matrix using copy constructor" << std::endl;
  matrix matrix_copy(matrix_default);
  matrix_copy.print();
  std::cout << std::endl;

  std::cout << "Reading in a matrix from file" << std::endl;
  matrix matrix_file;
  matrix_file.open("../input/a.txt");
  std::cout << "Read in matrix: " << std::endl;
  matrix_file.print();
  std::cout << std::endl;

  std::cout << "Testing * operator and = operator" << std::endl;
  matrix matrix_assign = matrix_default * matrix_copy;
  matrix_assign.print();
  std::cout << std::endl;

  std::cout << "Splitting first matrix into " << NUM_MATRICES << " separate matrices" << std::endl;
  matrix* matrices;
  matrices = matrix_default.split(NUM_MATRICES);
  if ( matrices == NULL ) {
    std::cerr << "Failed to split matrix" << std::endl;
    return 1;
  }
  for ( int matrix_index = 0; matrix_index < NUM_MATRICES; matrix_index++ ) {
    std::cout << "Matrix[" << matrix_index << "]:" << std::endl;
    matrices[matrix_index].print();
    std::cout << std::endl;
  }

  std::cout << "Merging matrices back together" << std::endl;
  matrix merge_matrix;
  merge_matrix.merge(matrices, NUM_MATRICES);
  std::cout << "Merged matrix:" << std::endl;
  merge_matrix.print();
  std::cout << std::endl;

  std::cout << "Shifting first row by -1" << std::endl;
  std::cout << "Original matrix:" << std::endl;
  matrix_default.print();
  std::cout << std::endl;
  matrix_default.shift_row(0, -1);
  std::cout << "Shifted matrix:" << std::endl;
  matrix_default.print();
  std::cout << std::endl;

  std::cout << "Shifting first col by -1" << std::endl;
  std::cout << "Original matrix:" << std::endl;
  matrix_default.print();
  std::cout << std::endl;
  matrix_default.shift_col(0, -1);
  std::cout << "Shifted matrix:" << std::endl;
  matrix_default.print();
  std::cout << std::endl;

  delete [] matrices;
  matrices = NULL;

  return 0;
}


