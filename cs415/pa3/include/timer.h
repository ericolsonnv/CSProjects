#ifndef TIMER_H
#define TIMER_H

#include <stdlib.h>
#include <sys/time.h>

double get_time(struct timeval t1);

#endif // TIMER_H