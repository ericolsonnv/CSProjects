#ifndef GENERATOR_H
#define GENERATOR_H

#include <stdlib.h>

#define MAX_SIZE 1000

int* rng(int seed, int count);

#endif