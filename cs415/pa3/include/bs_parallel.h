#ifndef BS_PARALLEL_H
#define BS_PARALLEL_H

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#include "generator.h"
#include "timer.h"

#define NUM_BUCKETS 10
#define MAX_MESSAGE_SIZE 100000

typedef struct Bucket {
  int size;
  int* nums;
} Bucket;

int intcmp(const void* first, const void* second);

#endif