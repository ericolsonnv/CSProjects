#include "../include/bs_sequential.h"

int main(int argc, char* argv[]) {
  int index;
  int total_nums;
  int max;
  int* unsorted;
  double elapsed_time;
  Bucket buckets[NUM_BUCKETS];
  struct timeval timer;

  // confirm that the correct number of arguments were supplied
  if ( argc != 2 ) {
    fprintf(stderr, "ERROR: main(): invalid number of arguments supplied\n");
    return 1;
  }

  // read in the size to sort
  sscanf(argv[1], "%d", &total_nums);

  // allocate memory for the unsorted array
  unsorted = (int *) calloc(total_nums, sizeof(*unsorted));

  // then allocate the appropriate memory to each bucket
  for ( index = 0; index < NUM_BUCKETS; index++ ) {
    buckets[index].size = 0;
    buckets[index].nums = (int *) calloc(total_nums, sizeof(int));
  }

  // generate unsorted list
  unsorted = rng(42, total_nums);

  // set min and max
  max = unsorted[0];
  for( index = 0; index < total_nums; index++ ) {
    if ( unsorted[index] > max ) {
      max = unsorted[index];
    }
  }

  // scatter list to buckets
  scatter(buckets, unsorted, total_nums, max);

  // time and sort
  gettimeofday(&timer, NULL);
  bs_sort(buckets);
  elapsed_time = get_time(timer);
  printf("%lf", elapsed_time);

  // gather buckets back into master array
  gather(buckets, unsorted);

  // print out sorted list
  // for( index = 0; index < total_nums; index++ ) {
  //   printf("%d\n", unsorted[index]);
  // }

  // free up allocated space
  for ( index = 0; index < NUM_BUCKETS; index++ ) {
    free(buckets[index].nums);
  }

  free(unsorted);
  return 0;
}

int intcmp(const void* first, const void* second) {
    int a = *((int*) first);
    int b = *((int*) second);

    if ( a == b ) {
      return 0;
    } else if ( a < b ) {
      return -1;
    } else {
      return 1;
    }
}

void scatter(Bucket* buckets, int* unsorted, int size, int max) {
  int index;
  int bucket_index;
  int partition_size = max / NUM_BUCKETS;

  for ( index = 0; index < size; index++ ) {
    for ( bucket_index = 0; bucket_index < NUM_BUCKETS; bucket_index++ ) {
      if ( unsorted[index] < (bucket_index + 1) * partition_size ) {
        buckets[bucket_index].nums[buckets[bucket_index].size] = unsorted[index];
        buckets[bucket_index].size++;
        break;
      } else if ( bucket_index == NUM_BUCKETS - 1 ) {
        buckets[bucket_index].nums[buckets[bucket_index].size] = unsorted[index];
        buckets[bucket_index].size++;
      }
    }
  }

  return;
}

void bs_sort(Bucket* buckets) {
  int bucket_index;

  for ( bucket_index = 0; bucket_index < NUM_BUCKETS; bucket_index++ ) {
    qsort(buckets[bucket_index].nums, buckets[bucket_index].size, sizeof(int), &intcmp);
  }

  return;
}

void gather(Bucket* buckets, int* sorted) {
  int index, bucket_index;
  int counter = 0;

  for ( bucket_index = 0; bucket_index < NUM_BUCKETS; bucket_index++ ) {
    for ( index = 0; index < buckets[bucket_index].size; index++ ) {
      sorted[counter] = buckets[bucket_index].nums[index];
      counter++;
    }
  }

  return;
}