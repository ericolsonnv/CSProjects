#include "../include/mandelbrot.h"

//*******************************//
// Calculates a single pixel in the mandelbrot set
// Globals:
//   MIN_REAL
//   MAX_REAL
//   MIN_IMAG
//   MAX_IMAG
//   MAX_ITERATIONS
// Arguments:
//   x location
//   y location
//   width of the image
//   height of the image
// Returns:
//   number of successful iterations
//*******************************//
int calculate_pixel(int x_current, int y_current, int width, int height) {
  float c_real;
  float c_imag;
  float z_real;
  float z_imag;
  float z2_real;
  float z2_imag;

  int n;

  c_real = MIN_REAL + x_current * ((MAX_REAL - MIN_REAL) / width);
  c_imag = MIN_IMAG + y_current * ((MAX_IMAG - MIN_IMAG) / height);

  z_real = 0.0;
  z_imag = 0.0;

  for ( n = 0; n < MAX_ITERATIONS; n++ ) {
    z2_real = z_real * z_real;
    z2_imag = z_imag * z_imag;

    if ( z2_real + z2_imag >= 4 ) {
      break;
    }

    z_imag = 2 * z_real * z_imag + c_imag;
    z_real = z2_real - z2_imag + c_real;
  }

  return n;
}

//*******************************//
// Selects a rgb color to reflect point in mandelbrot set
// Globals:
//   MAX_ITERATIONS
// Arguments:
//   reference pointer to red value
//   reference pointer to green value
//   reference pointer to blue value
//   integer n representing number of iterations
// Returns:
//   nothing
//*******************************//
void get_color(unsigned char* red, unsigned char* green, unsigned char* blue, int n) {
  if ( n == MAX_ITERATIONS) { // make the center of the plot black
    *red = 0;
    *green = 0;
    *blue = 0;
  } else if ( n * 10 < MAX_ITERATIONS ) { // make the outer edges and surrounding area have a purple tint
    *red = n * 6;
    *green = n * 0;
    *blue = n * 10;
  } else { // make the immediate edges white
    *red = 255;
    *green = 255;
    *blue = 255;
  }
}
