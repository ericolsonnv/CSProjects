// sequential.c
//
// Generates a mandelbrot set sequentially and outputs it to a .ppm

#include "../include/sequential.h"

//*******************************//
// Creates mandelbrot set image based on user specifications
// Globals:
//   none
// Arguments:
//   number of command line arguments
//   list of command line arguments
// Returns:
//   0 :- upon successful generation
//   1 :- if an invalid number of arguments were supplied
//*******************************//
int main(int argc, char* argv[]) {
  double elapsed_time;

  char* image;

  int width, height, image_size;
  int x_current, y_current;
  int index, n;

  unsigned char red, green, blue;

  struct timeval timer;

  FILE* fp;

  // verify that 2 extra arguments were supplied
    // the first should be the image size ( x = y )
    // the second should be the image to save to
  if( argc != 3 ) {
    fprintf(stderr, "main: invalid number of arguments\n");
    return 1;
  }

  // read in the image size
  sscanf(argv[1], "%d", &width);
  sscanf(argv[1], "%d", &height);
  image_size = width * height * 3;

  // open up the file specified to write the image to
  fp = fopen(argv[2], "wb");
  fprintf(fp,"P6\n%d %d\n255\n", width, height);

  // allocate memory for image (x3 due to rgb)
  image = (char*) malloc (image_size);

  // start timer
  gettimeofday(&timer, NULL);

  // begin calculations
  for ( y_current = 0; y_current < height; y_current++ ) {
    for ( x_current = 0; x_current < width; x_current++ ) {
      n = calculate_pixel(x_current, y_current, width, height);

      get_color(&red, &green, &blue, n);

      image[(x_current + (y_current * height)) * 3 + 0] = red;
      image[(x_current + (y_current * height)) * 3 + 1] = green;
      image[(x_current + (y_current * height)) * 3 + 2] = blue;
    }
  }
  // end timer
  elapsed_time = get_time(timer);
  printf("%lf", elapsed_time);

  // write image array to file
  for ( index = 0; index < image_size; index++ ) {
    fputc(image[index], fp);
  }

  // close file and free up allocations
  fclose(fp);
  free(image);
  image = NULL;

  return 0;
}
