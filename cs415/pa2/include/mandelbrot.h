#ifndef MANDELBROT_H
#define MANDELBROT_H

#define MIN_REAL -2.0
#define MAX_REAL  2.0
#define MIN_IMAG -2.0
#define MAX_IMAG  2.0
#define MAX_ITERATIONS 255

int calculate_pixel(int x_current, int y_current, int width, int height);
void get_color(unsigned char* red, unsigned char* green, unsigned char* blue, int n);

#endif // MANDELBROT_H