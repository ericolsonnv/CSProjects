#ifndef SEQUENTIAL_H
#define SEQUENTIAL_H

#include <stdio.h>
#include <stdlib.h>

#include "mandelbrot.h"
#include "timer.h"

void get_color(unsigned char* red, unsigned char* green, unsigned char* blue, int n);

#endif // SEQUENTIAL_H
