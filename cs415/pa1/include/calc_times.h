#ifndef RUN_H
#define RUN_H_

#include <stdio.h>
#include "mpi.h"

#define MASTER 0
#define NUM_CORES 8
#define NUM_TESTS 10000

void time_message();

#endif // RUN_H
