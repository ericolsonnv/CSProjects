#include <unistd.h>
#include "../include/calc_times.h"

void time_message() {
  int world_rank, world_size, index;
  double times[NUM_TESTS], average_time = 0;
  double t_send = 0, t_recv = 0;
  char hostname[MPI_MAX_PROCESSOR_NAME];

  // Initialize the MPI environment
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Verify correct size
  if( world_size != 2 ) {
    fprintf(stderr, "ERROR: send_local: world size must be three (size given: %d)\n", world_size);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  for( index = 0; index < NUM_TESTS; index++ )
  {
    if( world_rank == 0 ) {
      t_send = MPI_Wtime();
      MPI_Send(times, index, MPI_INT, 1, 0, MPI_COMM_WORLD);
    } else {
      MPI_Recv(times, index, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if ( world_rank == 0 ) {
      t_recv = MPI_Wtime();
      times[index] = t_recv - t_send;
      average_time += times[index];
      printf("%f\n", times[index]);
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);

  MPI_Finalize();
}
