#!/bin/bash

#SBATCH -n 2
#SBATCH --mem=1024MB
#SBATCH --time=00:01:00
#SBATCH --output=../log/one_box.log

srun ../bin/calc_times_test
