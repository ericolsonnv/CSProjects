#!/bin/bash

#SBATCH -n 2
#SBATCH -N 2
#SBATCH --mem=1024MB
#SBATCH --time=00:05:00
#SBATCH --output=../log/two_box.log

srun ../bin/calc_times_test
