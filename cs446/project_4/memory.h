/**************************************************************************/
// Description: Library for handling memory operations
// Author: Eric Olson
// Version: 1.1
// Date Last Modified: 11/8/16
/**************************************************************************/

#ifndef MEMORY_H_
#define MEMORY_H_

#include <stdio.h>
#include <stdlib.h>
#include "cfgparse.h"
#include "mdparse.h"

unsigned int AllocateMemory(config *system, int requested_memory);
unsigned int decimal_to_hex(unsigned int input);

#endif // MEMORY_H_
