/**************************************************************************/
// Description: Library for handling timing operations
// Author: Eric Olson
// Version: 1.0
// Date Last Modified: 10/5/16
/**************************************************************************/

#ifndef TIMER_H_
#define TIMER_H_

#include <time.h>
#include <stdio.h>

double timestamp(int wait_time_ms);

#endif // TIMER_H_
