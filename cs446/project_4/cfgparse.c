#include "cfgparse.h"

// Function to read in a configuration file
// Input:   String denoting filename,
//
// Outcome: Data interpreted from the file will be stored and returned
//          in a config object
//
// Notes: This method *works* but it's pretty ugly.
//
config read_config(char filename[MAX_SIZE])
{
  FILE *file;
  config configuration;
  char *buffer;
  char valid_head[MAX_SIZE] = "Start Simulator Configuration File";
  char valid_tail[MAX_SIZE] = "End Simulator Configuration File";

  // allocate memory to read in each line
  buffer = (char *) malloc(MAX_SIZE);
  if( buffer == NULL )
  {
    printf("[ERROR] Insufficient memory. Exit 1.\n");
    exit(1);
  }

  // open the config file
  if( !file_exists(filename) )
  {
    printf("[ERROR] Configuration file: %s does not exist. Exit 1.\n", filename);
    exit(1);
  }
  file = fopen(filename, "r");

  // verify header
  get_line(file, buffer, ':');
  if( strstr(buffer, valid_head) == NULL )
  {
    printf("[ERROR] Invalid configuration file. Exit 1.\n");

    exit(1);
  }

  configuration.allocated_blocks = 0;
  
  // continue reading in the rest of the file
  // look for specific key phrases
  while( get_line(file, buffer, ':') != EOF && strstr(buffer, valid_tail) == NULL )
  {
    if( strcmp(buffer, "Version/Phase:") == 0 )
    {
      fscanf (file, "%f", &configuration.version);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "File Path:") == 0 )
    {
      fscanf (file, "%s", configuration.meta_file_path);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Processor Quantum Number:") == 0 )
    {
      fscanf (file, "%d", &configuration.processor_quantum);
      get_line(file, buffer, '\n'); // move on to the next line      
    }
    else if( strcmp(buffer, "CPU Scheduling Code:") == 0 )
    {
      fscanf (file, "%s", configuration.processor_scheduling_code);
      get_line(file, buffer, '\n'); // move on to the next line      
    }
    else if( strcmp(buffer, "Processor cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.processor_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Monitor display time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.monitor_display_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Hard drive cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.hard_drive_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Printer cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.printer_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Keyboard cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.keyboard_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Memory cycle time (msec):") == 0 )
    {
      fscanf (file, "%d", &configuration.memory_cycle_time);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "System memory (kbytes):") == 0 )
    {
      fscanf (file, "%d", &configuration.physical_memory);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Memory block size (kbytes):") == 0 )
    {
      fscanf (file, "%d", &configuration.block_size);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Printer quantity:") == 0 )
    {
      fscanf (file, "%d", &configuration.num_printers);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Hard drive quantity:") == 0 )
    {
      fscanf (file, "%d", &configuration.num_hdds);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Log:") == 0 )
    {
      fscanf (file, "%s", configuration.log_type); // read over the first two words
      fscanf (file, "%s", configuration.log_type);
      fscanf (file, "%s", configuration.log_type); // this is the word we want

      get_line(file, buffer, '\n'); // move on to the next line
    }
    else if( strcmp(buffer, "Log File Path:") == 0 )
    {
      fscanf (file, "%s", configuration.log_path);
      get_line(file, buffer, '\n'); // move on to the next line
    }
    else
    {
      printf("Unknown configuration value: '%s' Skipping...\n", buffer);
      get_line(file, buffer, '\n'); // move on to the next line
    }

  }
  fclose(file);
  fflush(file);
  free(buffer);

  return configuration;
}
