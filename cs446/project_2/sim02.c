#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "file.h"
#include "cfgparse.h"
#include "mdparse.h"
#include "pcb.h"
#include "timer.h"

void start_os(config configuration, meta_data *mdata);

int main(int argc, char* argv[])
{
  config main_config;
  meta_data mdata[MAX_SIZE];

  // check that the program was ran correctly
  if( argc != 2 )
  {
    printf("[ERROR] Invalid number of arguments given. Exit 1.\n");
    return 1;
  }

  // check that the file specified exists
  if( !file_exists(argv[1]) )
  {
    printf("[ERROR] Configuration file does not exist. Exit 1.\n");
    return 1;
  }

  // read in the configuration file
  main_config = read_config(argv[1]);

  // read in the meta data
  read_data(mdata, main_config);

  // start that stuff
  start_os(main_config, mdata);

  return 0;
}


// Function to begin the running of the os
// Input:   Config to help guide paths and values
//          Array of meta data to load into processes
// Outcome: PCBs are loaded up from previously loaded meta data
//          and the processes rotate through their states
//
void start_os(config configuration, meta_data *mdata)
{
  pcb processes[MAX_SIZE];
  int index = 0;
  int p_num = 0;
  FILE* fstream = NULL;

  // make sure the path isn't null and that we should be opening a file
  if( configuration.log_path != NULL && (!strcmp(configuration.log_type, "File") || !strcmp(configuration.log_type, "Both")) )
  {
    fstream = fopen(configuration.log_path, "w+");
  }

  // loop until end of meta data block
  while( index < MAX_SIZE )
  {
    p_num = p_create(&processes[index], mdata[index], p_num, configuration.physical_memory);
    p_ready(&processes[index]);

    // verify that the process is ready to be ran
    if( !strcmp(processes[index].state, "READY") )
    {
      p_run(&processes[index], configuration, fstream);
    }
    else
    {
      printf("[ERROR] Next process in queue not ready. Moving to the next process in queue.\n");
    }

    // if the os end command is reached, end the loop
    if ( mdata[index].code == 'S' && !strcmp(mdata[index].descriptor, "end") )
    {
      break;
    }
    index++;
  }

  // this really shouldn't happen unless the appropriate end call wasn't found
  if( index >= MAX_SIZE )
  {
    printf("[WARNING] Meta data reached capacity\n");
  }

  // make sure the file has been opened
  if( fstream != NULL )
  {
    fflush(fstream);
    fclose(fstream);
  }
}
