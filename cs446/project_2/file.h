/**************************************************************************/
// Description: Library for handling file operations
// Author: Eric Olson
// Version: 1.0
// Date Last Modified: 10/5/16
/**************************************************************************/
#ifndef MAX_SIZE
#define MAX_SIZE 1024
#endif // MAX_SIZE

#ifndef FILE_H_
#define FILE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int get_line(FILE *stream, char *line, char delim);
int file_exists();

#endif // FILE_H_
