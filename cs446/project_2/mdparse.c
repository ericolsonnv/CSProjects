#include "mdparse.h"

// Function to get a line from a file up to specified delimeter or newline
// Input:   Meta data pointer to store to
//          Configuration data to determine file path and cycle times
// Outcome: Meta data is passed back by reference
//
void read_data(meta_data *mdata, config configuration)
{
  FILE *file;
  char *buffer;
  char *cat_buffer;
  char *filename = configuration.meta_file_path;
  char peek;
  int buffer_len;
  int index = 0;
  int desc_success;
  char valid_head[MAX_SIZE] = "Start Program Meta-Data Code:\n";

  // allocate memory to read in each line
  buffer = (char *) malloc(MAX_SIZE);
  cat_buffer = (char *) malloc(MAX_SIZE);
  if( buffer == NULL || cat_buffer == NULL )
  {
    printf("[ERROR] Insufficient memory. Exit 1.\n");
    exit(1);
  }

  // open the config file
  if( !file_exists(filename) )
  {
    printf("[ERROR] Meta-data file: %s does not exist. Exit 1.\n", filename);
    exit(1);
  }
  file = fopen(filename, "r");
  get_line(file, buffer, ';');

  // verify header
  if( strcmp(buffer, valid_head) )
  {
    printf("[ERROR] Invalid meta-data file. Exit 1.\n");
    exit(1);
  }

  // read in one string at a time
  while( fscanf(file, "%s", buffer) != EOF && strcmp(buffer, "End") )
  {
    if( index == MAX_SIZE )
    {
      printf("[ERROR] Meta data file exceeded %d entries. Not enough memory allocated to continue.\n", MAX_SIZE);
      exit(1);
    }

    buffer_len = strlen(buffer); // set string length for terminator checking
    peek = getc(file); // peek at the next character

    // check for the following:
    //   * accepted terminators/delimeters are not present
    //     * this includes periods, colons, and semi-colons
    //   * also check for a space after the string
    // if both conditions are met, then chances are we have a multi-word string
    // to read in, thus we should append the following string to the current buffer
    if( buffer[buffer_len - 1] != ':' && buffer[buffer_len - 1] != ';'
        && buffer[buffer_len - 1] != '.' && peek == ' '                )
    {
      fscanf(file, "%s", cat_buffer); // read in the next string
      buffer[buffer_len] = ' '; // replace the current terminator with a space
      buffer[buffer_len + 1] = '\0'; // append a terminator after to recreate string
      strcat(buffer, cat_buffer); // concatenate the two strings
    }
    else
    {
      ungetc(peek, file); // put the "peeked" character back if the above case didn't occur
    }

    mdata[index].code = get_code(buffer);
    desc_success = get_descriptor(buffer, mdata[index].descriptor);
    mdata[index].cycles = get_cycles(buffer);
    mdata[index].run_time = get_run_time(configuration, mdata[index]);

    // verify that none of the meta data components errored out
    if( mdata[index].code != -1 &&
        desc_success != -1 &&
        mdata[index].cycles != -1        )
    {
      index++;
    }
    else
    {
      printf("Invalid meta data encountered. Skipping.\n");
    }
  }

  fclose(file); // close the file
  fflush(file);

  free(buffer);
  free(cat_buffer);

  return;
}

// Function to get meta data code
// Input:   String to obtain meta data code from
// Outcome: Character containing the meta data code is returned
//          -1 is returned if an invalid string is passed
//
char get_code(char* string)
{
    if( string == NULL )
    {
      return -1;
    }
    else
    {
      return string[0];
    }
}

// Function to get descriptor
// Input:   Char* to obtain descriptor from
//          Char* to store descriptor to
// Outcome: Descriptor is passed back by reference
//          Boolean is returned to represent success
//
int get_descriptor(char* string, char* descriptor)
{
  int string_index = 2; // start at the 3rd letter
  int descriptor_index = 0; // start at the first location

  if( string == NULL )
  {
    return -1;
  }

  // read until descriptor delim
  while( string[string_index] != ')' && string_index < MAX_SIZE )
  {
    descriptor[descriptor_index] = string[string_index];
    string_index++;
    descriptor_index++;
  }

  descriptor[descriptor_index] = '\0';
  return 0;
}

// Function to get an integer # of cycles from a string
// Input:   Char* to obtain cycles from
// Outcome: Cycle # is is returned
//          Cycle # of -1 represents an error
//
int get_cycles(char* string)
{
    int index = 0;
    int cycles_index = 0;
    int size = 0;
    int cycles[MAX_SIZE];
    int total = 0;

    if( string == NULL )
    {
      return -1;
    }

    // loop until the expected cycles location
    while( string[index] != ')' )
    {
      index++;
    }
    index++;

    // loop over all consecutive available numbers
    while( string[index] >= '0' && string[index] <= '9' )
    {
      cycles[cycles_index] = string[index] - '0';
      index++;
      cycles_index++;
    }
    size = cycles_index;
    cycles_index--;

    // convert int array into integer
    for( index = 0; index < size; index++)
    {
      total += cycles[index] * power(10, cycles_index);
      cycles_index--;
    }

    return total;
}

// Function to get meta data run time
// Input:   Configuration file to get global times from
//          Meta data object to modify
// Outcome: Run time for the specified meta data object is returned
//          Defaults to 0 if descriptors aren't recognized
//
int get_run_time(config configuration, meta_data mdata)
{
  int run_time;

  switch(mdata.code)
  {
    case 'P':
      run_time = configuration.processor_cycle_time * mdata.cycles;
      break;

    case 'M':
      run_time = configuration.memory_cycle_time * mdata.cycles;
      break;

    case 'I':
      if( !strcmp(mdata.descriptor, "keyboard"))
      {
        run_time = configuration.keyboard_cycle_time * mdata.cycles;
      }
      else if( !strcmp(mdata.descriptor, "hard drive") )
      {
        run_time = configuration.hard_drive_cycle_time * mdata.cycles;
      }
      else
      {
        run_time = 0;
      }
      break;

    case 'O':
      if( !strcmp(mdata.descriptor, "monitor"))
      {
        run_time = configuration.monitor_display_time * mdata.cycles;
      }
      else if( !strcmp(mdata.descriptor, "printer") )
      {
        run_time = configuration.printer_cycle_time * mdata.cycles;
      }
      else if( !strcmp(mdata.descriptor, "hard drive") )
      {
        run_time = configuration.hard_drive_cycle_time * mdata.cycles;
      }
      else
      {
        run_time = 0;
      }
      break;

    default:
      run_time = 0;
      break;
  }

  return run_time;
}

// Function to calculate exponents
// Input:   Integer base
//          Integer power
// Outcome: The base to the exponent power is returned
//
// Note:    I probably could have just used atoi
//
int power(int base, int power)
{
  int index;

  if( power == 0 )
  {
    return 1;
  }

  for( index = 1; index < power; index++ )
  {
      base *= base;
  }

  return base;
}
