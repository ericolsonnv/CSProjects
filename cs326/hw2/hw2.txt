1.
  a)
    (define (is-set? set)
	    (if (= (length set) 1) ; a list of size 1 is a set
	        #t
		    (if (member (car set) (cdr set)) ; else if the first element is found elsewhere in the list
		        #f
		        (is-set? (cdr set)) ) ; else check the cdr
	    )
    )

  b)
    (define (make-set lst)
      (cond 
        ((null? lst) 
                '()) ; if empty list return empty list
        ((member (car lst) (cdr lst)) ; if the item exists elsewhere in the list
                 (make-set (cons (car lst) (remove (car lst) lst))))  
        (else 
              lst) ; return the list
      )
    )
    
  c)
    (define (subset? A S)
      (if (is-set? A) 
        (if (null? A) #t 
          (if (member (car A) S) (subset? (cdr A) S) 
            #f 
          )
        )
        #f
      )
    )
  
  d)
  
  
  e)
  
  
2.
  a)
    (define (tree-member? x T)
      (cond (( eq? (length T) 0) #f)
            (else
                 (cond (( eq? x (car T)) #t)
                       (else
                           (cond ((> x (car T)) (tree-member? x (car (cdr(cdr T)))))
                       (else (tree-member? x (car (cdr T))))
                 ))))))

  b)
    (define (preorder T)
      (if (null? T)
         T
        (append(append(list(car T))
                  (preorder(car (cdr T))))                        
                (preorder(car (cdr(cdr T)))))
               ))
               
  c)
    (define (inorder T)
      (if (null? T)
         T
        (append(append(inorder(car (cdr T)))
                  (list(car T)))                        
                (inorder(car (cdr(cdr T)))))
               ))       
         
3.
    (define (deep-delete V L)
      (cond
        ((null? L) 
          '())
        ((list? (car L)) 
          (append (list(deep-delete V (car L))) (deep-delete V (cdr L))))
        ((equal? V (car L)) 
          (deep-delete V (cdr L)))
        (else 
          (cons (car L) (deep-delete V (cdr L))))
      )
    )
   
