package ColorSampler;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;

public class SamplerWindow extends JFrame
{
  int redVal = 0;
  int blueVal = 0;
  int greenVal = 0;

  public SamplerWindow()
  {
    FileHandler fh = new FileHandler();
    int lines = fh.CountLines("colors.txt");
    ColorData[] cData = fh.ReadColorData("colors.txt");
    
    String[] colorsList = new String[lines];
    for( int i = 0; i < lines; i++ )
    {
      colorsList[i] = cData[i].GetColor();
    }
    
    setTitle("Color Sampler");
    setSize(1250, 600);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
    setLayout(new GridLayout(1, 2));

    // create panels for each half of frame
    JPanel leftPanel = new JPanel(new GridLayout(3, 1));
    JPanel rightPanel = new JPanel();
    
    // panels for left half of frame
    JPanel palettePanel = new JPanel(new GridLayout(1, 1, 25, 25));
    JPanel rgbPanel = new JPanel(new GridLayout(3, 4, 25, 25));
    JPanel buttonPanel = new JPanel(new FlowLayout());

    // panels for right half of frame
    JPanel colorsPanel = new JPanel();

    //create labels to use in rgb panels
    JLabel redLabel = new JLabel("Red:");
    JLabel blueLabel = new JLabel("Blue:");
    JLabel greenLabel = new JLabel("Green:");

    JLabel redField = new JLabel(String.valueOf(redVal));
    redField.setBackground(Color.white);
    redField.setOpaque(true);

    JLabel blueField = new JLabel(String.valueOf(blueVal));
    blueField.setBackground(Color.white);
    blueField.setOpaque(true);

    JLabel greenField = new JLabel(String.valueOf(greenVal));
    greenField.setBackground(Color.white);
    greenField.setOpaque(true);
        
    

    // create list for right frame
    JList<String> colorList = new JList<>(colorsList);
    
    ListSelectionListener listSelectionListener = new ListSelectionListener() {
      public void valueChanged(ListSelectionEvent listSelectionEvent) {
        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        redVal = cData[index].GetRVal();
        greenVal = cData[index].GetGVal();
        blueVal = cData[index].GetBVal();
        
        redField.setText(String.valueOf(redVal));
        greenField.setText(String.valueOf(greenVal));
        blueField.setText(String.valueOf(blueVal));

        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));
      }
    };
    colorList.addListSelectionListener(listSelectionListener);

    // create buttons to use in frame
    JButton rMinusButton = new JButton("-");
    rMinusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e) 
      {         
        if( redVal > 0 )
        {
          redVal -= 5; 
          redField.setText(String.valueOf(redVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));

        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetRVal(redVal);
      } 
    } );

    JButton rPlusButton = new JButton("+");
    rPlusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        if( redVal < 255 )
        {
          redVal += 5; 
          redField.setText(String.valueOf(redVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));

        int index = colorList.getSelectedIndex();
        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetRVal(redVal);
      } 
    } );

    JButton gMinusButton = new JButton("-");
    gMinusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        if( greenVal > 0 )
        {
          greenVal -= 5; 
          greenField.setText(String.valueOf(greenVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));
        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetGVal(greenVal);

      } 
    } );
  
    JButton gPlusButton = new JButton("+");
    gPlusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        if( greenVal < 255 )
        {
          greenVal += 5; 
          greenField.setText(String.valueOf(greenVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));
        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetGVal(greenVal);

      } 
    } );
  
    JButton bMinusButton = new JButton("-");
    bMinusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        if( blueVal > 0 )
        {
          blueVal -= 5; 
          blueField.setText(String.valueOf(blueVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));
        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetBVal(blueVal);

      } 
    } );
  
    JButton bPlusButton = new JButton("+");
    bPlusButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 

        if( blueVal < 255 )
        {
          blueVal += 5; 
          blueField.setText(String.valueOf(blueVal));           
        }
        palettePanel.setBackground(new Color(redVal, greenVal, blueVal));
        int index = colorList.getSelectedIndex();

        if( index == -1 )
        {
          return;
        }
        
        cData[index].SetBVal(blueVal);

      } 
    } );
  
    JButton saveButton = new JButton("Save");
    saveButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        fh.SaveColorData(cData, "colors.txt");
      }
    } );

    JButton resetButton = new JButton("Reset");
    resetButton.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent e)
      { 
        ColorData[] cNewData = fh.ReadColorData("colors.txt");
        for( int i = 0; i < cNewData.length; i++ )
        {
          cData[i].SetColor( cNewData[i].GetColor() );
          cData[i].SetRVal( cNewData[i].GetRVal() );
          cData[i].SetGVal( cNewData[i].GetGVal() );
          cData[i].SetGVal( cNewData[i].GetBVal() );          
        }
      } 
    } );
    
    // customize left sub-panels
    palettePanel.setBackground(Color.black);
    
    rgbPanel.add(redLabel);
    rgbPanel.add(redField);
    rgbPanel.add(rMinusButton);
    rgbPanel.add(rPlusButton);

    rgbPanel.add(greenLabel);
    rgbPanel.add(greenField);
    rgbPanel.add(gMinusButton);
    rgbPanel.add(gPlusButton);

    rgbPanel.add(blueLabel);
    rgbPanel.add(blueField);
    rgbPanel.add(bMinusButton);
    rgbPanel.add(bPlusButton);

    buttonPanel.add(saveButton);
    buttonPanel.add(resetButton);
        
    // add sub-panels to left panel
    leftPanel.add(palettePanel);
    leftPanel.add(rgbPanel);
    leftPanel.add(buttonPanel);

    // add list to panel
    rightPanel.add(colorList);
    
    add(leftPanel);
    add(rightPanel);
  }
}
