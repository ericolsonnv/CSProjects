package ColorSampler;

public class ColorData
{
  private String color;
  private int r;
  private int g;
  private int b;

  public ColorData()
  {
    color = "nil";
    r = 0;
    g = 0;
    b = 0;
  }

  public void SetColor(String c)
  {
    color = c;
    return;
  }

  public void SetRVal(int rval)
  {
    r = rval;
    return;
  }

  public void SetGVal(int gval)
  {
    g = gval;
    return;
  }

  public void SetBVal(int bval)
  {
    b = bval;
    return;
  }
  
  public String GetColor()
  {
    return color;
  }

  public int GetRVal()
  {
    return r;
  }

  public int GetGVal()
  {
    return g;
  }

  public int GetBVal()
  {
    return b;
  }

  public String toString()
  {
    return color;
  }
}
